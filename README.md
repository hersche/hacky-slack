## Hacky-slack

A small slack-bot-script written in python3 with a bunch of libs.

The name is a small word-play with hacky-sack, a tradename for footbags.

The main-purpose was to have a reminder-bot which can do something that a lot of bots didn't - remind for example always at the last thuesday at month.

It can also reminder 'once' or weekly, etc. Use `@botname help me` to get a overview.

Also, librarys like pyjoke, art, xkcd, wikipedia, country-info and some more has been used for several offtopic-functions.

To initialise, do a

    export SLACK_BOT_TOKEN="xoxb-YOUR-SLACK-BOTTOKEN"

    pip3 install -r requirements.txt

and start the script with

    python3 start.py
    
The database set up itself as db.sqlite3. It's only purpose is to track the reminders and it's creators. Also, a logfile will be created, called hacky-slacky.log. It doesn't collect any personal data but let you analyse the app. Most can be disabled by decrease the report-level to error or so (logger.py).
    
The script is able to work on a raspberry-pi and odroid xu-4. However, you need to compile Python 3.7 yourself and make shure it has sqlite3-support. It's not the purpose of this project to explain this, but a small tutorial will maybe follow in future.


There are out-commented lines which you can enable to activate the chatterbot-library, where the idea is, that it fetches all non-prepared answers. However, it doesn't run on rpi nor odroid and as it's based on ML, it takes up to 10min to answer (with a i7 7. gen) - so it's disabled for now.
