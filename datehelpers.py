import calendar
from datetime import datetime
from dateutil import tz
from logger import *

def getMonthResponse(response,weekPart,startdatesplit,iz):
    if(int(weekPart)<4):
        response += "\r\n"+goToMonthPlace(weekPart,startdatesplit,datetime(iz.year,iz.month,calendar.monthrange(iz.year, iz.month)[1]))[1]
        if iz.month+1<12:
            response += "\r\n"+goToMonthPlace(weekPart,startdatesplit,datetime(iz.year,iz.month+1,calendar.monthrange(iz.year, iz.month+1)[1]))[1]
        else:
            response += "\r\n"+goToMonthPlace(weekPart,startdatesplit,datetime(iz.year,newyearmonth,calendar.monthrange(iz.year, newyearmonth)[1]))[1]
            newyearmonth += 1
        if iz.month+2<12:
            response += "\r\n"+goToMonthPlace(weekPart,startdatesplit,datetime(iz.year,iz.month+2,calendar.monthrange(iz.year, iz.month+2)[1]))[1]
        else:
            response += "\r\n"+goToMonthPlace(weekPart,startdatesplit,datetime(iz.year,newyearmonth,calendar.monthrange(iz.year, newyearmonth)[1]))[1]
            newyearmonth += 1
        if iz.month+3<12:
            response += "\r\n"+goToMonthPlace(weekPart,startdatesplit,datetime(iz.year,iz.month+3,calendar.monthrange(iz.year, iz.month+3)[1]))[1]
        else:
            response += "\r\n"+goToMonthPlace(weekPart,startdatesplit,datetime(iz.year,newyearmonth,calendar.monthrange(iz.year, newyearmonth)[1]))[1]
            newyearmonth += 1
    else:
        response += "\r\n"+goReverseToMonthPlace(weekPart,startdatesplit,datetime(iz.year,iz.month,calendar.monthrange(iz.year, iz.month)[1]))[1]
        if iz.month+1<12:
            response += "\r\n"+goReverseToMonthPlace(weekPart,startdatesplit,datetime(iz.year,iz.month+1,calendar.monthrange(iz.year, iz.month+1)[1]))[1]
        else:
            response += "\r\n"+goReverseToMonthPlace(weekPart,startdatesplit,datetime(iz.year,newyearmonth,calendar.monthrange(iz.year, newyearmonth)[1]))[1]
            newyearmonth += 1
        if iz.month+2<12:
            response += "\r\n"+goReverseToMonthPlace(weekPart,startdatesplit,datetime(iz.year,iz.month+2,calendar.monthrange(iz.year, iz.month+2)[1]))[1]
        else:
            response += "\r\n"+goReverseToMonthPlace(weekPart,startdatesplit,datetime(iz.year,newyearmonth,calendar.monthrange(iz.year, newyearmonth)[1]))[1]
            newyearmonth += 1
        if iz.month+3<12:
            response += "\r\n"+goReverseToMonthPlace(weekPart,startdatesplit,datetime(iz.year,iz.month+3,calendar.monthrange(iz.year, iz.month+3)[1]))[1]
        else:
            response += "\r\n"+goReverseToMonthPlace(weekPart,startdatesplit,datetime(iz.year,newyearmonth,calendar.monthrange(iz.year, newyearmonth)[1]))[1]
            newyearmonth += 1
    return response

def goReverseToMonthPlace(place,theDate,iz=None):
    logger.debug("Start goReverseToMonthPlace")
    i=0
    if(iz==None):
        iz = datetime.now()
    postIt = False
    
    denn = datetime(int(theDate[2]),int(theDate[1]),int(theDate[0])).weekday()
    lastDayOfMonth = calendar.monthrange(iz.year,iz.month)[1]
    weekDay = iz.weekday()
    currentWeekDay = datetime(iz.year,iz.month,lastDayOfMonth).weekday()
    # wantedDate = datetime(iz.year,iz.month,lastDayOfMonth)
    place = int(place)
    message = ""
    if(place==5):
        place = -1
    if(place==4):
        place = 1
    while(currentWeekDay!=denn):
        lastDayOfMonth -= 1
        currentWeekDay = datetime(iz.year,iz.month,lastDayOfMonth).weekday()
        # wantedDate = datetime(iz.year,iz.month,lastDayOfMonth)
    if(lastDayOfMonth==iz.day and denn==currentWeekDay and place==-1):
        message = "It's now! Last day of the month "+str(iz.month)+" for weekday "+str(currentWeekDay)+" is "+str(lastDayOfMonth)+"."+str(iz.month)+"."+str(iz.year)
        logger.debug("Found date early, return")
        return [True,message]
    if(place==-1):
        message = "Last day of the month "+str(iz.month)+" for weekday "+str(currentWeekDay)+" is "+str(lastDayOfMonth)+"."+str(iz.month)+"."+str(iz.year)
        logger.debug("return false?? "+str(lastDayOfMonth))
        logger.debug("Not found date, return false")
        return [False,message]
    logger.debug("Pass prechecks, go into while's")
    logger.debug(lastDayOfMonth)    
    while i<place:
        if(6>4):
            logger.debug("make round of 2")
            # this was once lastDayOfMonth -1, but as monthly-4 and monthly-5 mean now the exact same thing, this is fine.
            lastDayOfMonth = calendar.monthrange(iz.year,iz.month)[1]
            currentWeekDay = datetime(iz.year,iz.month,lastDayOfMonth).weekday()
            while(currentWeekDay!=denn):
                lastDayOfMonth -= 1
                currentWeekDay = datetime(iz.year,iz.month,lastDayOfMonth).weekday()
                logger.debug("search lastday "+str(lastDayOfMonth)+" (got "+str(currentWeekDay)+", want "+str(denn)+")")
            message = "Second-Last day of the month "+str(iz.month)+" for weekday "+str(currentWeekDay)+" is "+str(lastDayOfMonth)+"."+str(iz.month)+"."+str(iz.year)
            #weekDay = datetime(iz.year,iz.month,iz.day).weekday()
            if(denn==currentWeekDay and lastDayOfMonth==iz.day):
                if(i==place-1):
                    postIt = True
                else:
                    postIt = False
        i += 1
    return [postIt,message]



def goToMonthPlace(place,theDate,iz=None):
    logger.debug("Start goToMonthPlace")
    i=0
    if(iz==None):
        iz = datetime.now()
    postIt = False
    weekDay = datetime(iz.year,iz.month,iz.day).weekday()
    roundWeekDay = datetime(iz.year,iz.month,iz.day).weekday()
    firstAndLast = calendar.monthrange(iz.year,iz.month)
    firstDay = 1
    targetWeekday = datetime(int(theDate[2]),int(theDate[1]),int(theDate[0])).weekday()
    stopper = False
    message = ""
    ii = 0
    place = int(place)
    while i<place and not stopper:
        internalCount = 0
        foundDay = 0
        responseFoundDay = 0
        while(firstDay+ii<=iz.day or i<place):
            logger.debug("first day + ii: "+str(firstDay)+"+"+str(ii)+" vs target-day" + str(iz.day))
            if(firstDay+ii>iz.day):
                logger.debug("use break to leave inner while")
                stopper=True
                break
            roundWeekDay = datetime(iz.year,iz.month,firstDay+ii).weekday()
            #weekDay = datetime(iz.year,iz.month,iz.day).weekday()
            #print("whut111")
            if(targetWeekday==roundWeekDay):
                if(internalCount>0):
                    logger.debug("weekday already found, do break (should not happen)")
                    break
                # logger.debug("outter round-i: "+str(i))
                if(i==place-1):
                    logger.debug("found date it, it's ready")
                    foundDay = firstDay+ii
                    postIt = True
                    
                else:
                    #logger.debug("not found date in inner while")
                    postIt = False
                internalCount += 1
                responseFoundDay = firstDay+ii
            else:
                #logger.debug("not found date in outter while")
                postIt = False
            ii += 1
            
        i += 1
    if(iz.day==foundDay):
        postIt = True
    message = "On the "+str(responseFoundDay)+"."+str(iz.month)+"."+str(iz.year)+" it should happen.."
    logger.debug("End goToMonthPlace")
    return [postIt,message]
