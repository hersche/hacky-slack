
import calendar
from datetime import datetime
from dateutil import tz
from logger import *

import re
import pyjokes
import xkcd
import sqlite3

from datehelpers import *

import wikipedia
import random
from art import *

import pyhibp
from sqlite3 import Error
from countryinfo import CountryInfo
from chuck import ChuckNorris
cn = ChuckNorris()
rows = None
laughCounter = 0
from difflib import SequenceMatcher

# this defines, how exact commands must be. 0.9 is the max highest recommandation as when commands use arguments, it's more save to left a bit. under 0.75 is not recommended, as it may prevent other commands from being executed - but have fun play around :)
diffPropability = 0.78

# https://stackoverflow.com/questions/17388213/find-the-similarity-metric-between-two-strings
def similar(a, b):
    # small ugly workaround to catch reminder-commands. 
    command = a
    if command.startswith("delete reminder ") or command.startswith("show reminder") or command.startswith("do remind"):
        return 0.0
    return SequenceMatcher(None, a, b).ratio()

# This method only exists to make the row-variable accessible the same variable from other python-files
def getRows():
    global rows
    updateReminders()
    return rows

def updateReminders():
    global rows
    conn3 = sqlite3.connect("./botdates.sql")
    cur = conn3.cursor()
    cur.execute("SELECT * FROM reminders")
    rows = cur.fetchall()
    cur.close()
    conn3.close()

def simplifyChannelName(channelName):
    if(channelName.startswith("<")):
        return channelName[2:].split("|")[0]
    if(channelName.startswith("@")):
        return channelName[1:]
    return channelName
def simplifyEmailName(channelName):
    if(channelName.startswith("<mailto")):
        return channelName[8:].split("|")[0]
    return channelName
def simplifyEmailId(channelName):
    if(channelName.startswith("<@")):
        return channelName[2:-1]
    return channelName


def intepretReaction(payload):
    global laughCounter
    response=""
    if('item_user' not in payload['data']):
        # it's a post of the bot!
        if payload['data']['reaction'] == 'exploding_head':
            response="Ha! i blowed your mind! :grin:"
        if payload['data']['reaction'] == 'grin' or  payload['data']['reaction'] == 'smile' or  payload['data']['reaction'] == 'slightly_smiling_face':
            randomised = int(random.getrandbits(3))
            if randomised == 0:
                response="Are you happy now? well, me too :smile:"
            elif randomised == 1:
                response=":slightly_smiling_face: a elixir for you! :vinzelixir:"
            elif randomised == 2:
                response=":wink: a cat with superpower and good music for that! :nyancat:"
            else:
                logger.debug("Ignore happy reaction")
        if payload['data']['reaction'] == 'joy':
            randomised = int(random.getrandbits(2))
            if randomised == '0':
                response="haha..? :face_with_rolling_eyes:"
            elif randomised == '1':
                response="Too bad i don't understand context :thinking_face:"
            else:
                logger.debug("Not answer.. pff")
        if payload['data']['reaction'] == 'rolling_on_the_floor_laughing' or payload['data']['reaction'] == 'sweat_smile':
            randomised = int(random.getrandbits(4))
            if randomised == 0:
                response=":cry:"
            elif randomised == 1:
                response=":smirk:"
            if randomised == 2:
                response="Too bad i don't understand context :thinking_face:"
            if randomised == 3:
                response=":sweat_smile:"
            else:
                logger.debug("ignore that laugh-reaction")
    return response

def intepretReminder(reminder,iz):
                 dateti = reminder[4].split()
                 theDate = dateti[0].split(".")
                 theTime = dateti[1].split(":")
                 reminderDateObject = datetime(int(theDate[2]),int(theDate[1]),int(theDate[0]))
                 lastDayOfMonth = calendar.monthrange(iz.year,iz.month)[1]
                 i=0
                 weekDay = datetime(iz.year,iz.month,lastDayOfMonth).weekday()
                 while(weekDay>4):
                     lastDayOfMonth -= 1
                     i += 1
                     weekDay = datetime(iz.year,iz.month,lastDayOfMonth).weekday()
                     if(weekDay<5):
                     #if(True):
                             insert = True
                     else:
                             insert = False
                     weekDay = datetime(iz.year,iz.month,lastDayOfMonth).weekday()
                 #print(iz.weekday())
                 postIt = False
                 if(reminder[3]=="once-weekday" and iz.hour==itiment(theTime[0]) and iz.minute==int(theTime[1]) and iz.day-i==int(theDate[0])):
                     postIt = True
                 if(reminder[3]=="once" and iz.hour==int(theTime[0]) and iz.minute==int(theTime[1]) and iz.day==int(theDate[0]) and iz.month==int(theDate[1]) and iz.year==int(theDate[2])):
                     postIt = True
                     # print("blaaaa" +str(reminder[2])[2:].split("|")[0])
                 if(reminder[3]=="weekly" and iz.hour==int(theTime[0]) and iz.minute==int(theTime[1])):
                     denn = datetime(int(theDate[2]),int(theDate[1]),int(theDate[0]))
                     if(denn.weekday()==iz.weekday()):
                             postIt = True
                 if(reminder[3]=="monthly" and iz.hour==int(theTime[0]) and iz.minute==int(theTime[1]) and iz.day==int(theDate[0])):
                     postIt = True
                 if(reminder[3]=="monthly-last-weekday" and iz.hour==int(theTime[0]) and iz.minute==int(theTime[1]) and iz.day==lastDayOfMonth):
                     postIt = True
                 if(reminder[3].startswith("monthly-") and not reminder[3]=="monthly-last-weekday" and iz.hour==int(theTime[0]) and iz.minute==int(theTime[1])):
                     spliters = reminder[3].split("-")
                     weekPart = spliters[1]
                     logger.info("check monthly")
                     logger.info(weekPart)
                     firstDayOfMonth = calendar.monthrange(iz.year,iz.month)[0]
                     denn = datetime(int(theDate[2]),int(theDate[1]),int(theDate[0]))
                     weekDay = denn.weekday()
                     weekDayCounter = denn.weekday()
                     if(weekPart=="1"):
                         postIt = goToMonthPlace(1,theDate)[0]
                     if(weekPart=="2"):
                         postIt = goToMonthPlace(2,theDate)[0]
                     if(weekPart=="3"):
                         postIt = goToMonthPlace(3,theDate)[0]
                        
                     if(weekPart=="4"):
                         postIt = goReverseToMonthPlace(5,theDate)[0]
                     if(weekPart=="5"):
                         postIt = goReverseToMonthPlace(5,theDate)[0]
                 return postIt

def intepretCommand(command,channel,data):
    """
        Executes bot command if the command is known
    """
    # Default response is help text for the user
    response = "Not sure what you mean. Try mention me and *help me* :slightly_smiling_face:"
    command = re.sub(r'\s+', ' ',command).replace(r'\xc2\xa0', ' ').replace('\xc2\xa0', ' ')
    command = command.lower()
    useDirectMessage = False
    #print(command)
    '''
    if not command.startswith("make a joke") and not command.startswith("make art") and not command.startswith("make small art") and not command.startswith("tell me a joke") and not command.startswith("tell a joke") and not command.startswith("help") and not command.startswith("show reminder") and not command.startswith("delete reminder ") and not command.startswith("country ") and not command.startswith("wikipedia ") and not command.startswith("hacked ") and not command.startswith("do remind") and not command.startswith("help"):
        print("No sys-command, so apply AI")
        response = str(bot.get_response(command))+"\n \n (This answer is unpredictect by \"AI\" - if you need help, typ \"help me\" after mention)"
        if response is None:
            print("AI-response was None")
            response='AI-fail'
        print("Answer in terminal:")
        print(response)
        '''
    # This is where you start to implement more commands!
    # if command.startswith("make a joke") or command.startswith("tell me a joke") or command.startswith("tell a joke"):
    if similar(command, "make a joke") > diffPropability:
        try:
            randomised = int(random.getrandbits(2))
            if command.split()[-1].isdigit():
                randomised = int(command.split()[-1])
            if randomised==0:
                response = pyjokes.get_joke(category='all')
            elif randomised==1:
                try:
                    response = str(cn.random())
                except Exception as e:
                    response = "chuck-error!! "+str(e) 
            else:
                comic = xkcd.getRandomComic()
                response = comic.getImageLink()
                response += ' Explaining: '+comic.getExplanation()+' :smirk:'
        except Exception as e:
            response = "error!! "+str(e)
    # if command.startswith("help") or command.startswith("help me"):
    elif similar(command, "help me") > diffPropability:
        response = ":smirk: *Mention me* and add commands like: \r\n -----------\r\n"
        response += "do remind channel <weekly|weekly-weekday|monthly|monthly-weekpart|once> dd.mm.yyyy hh:mm message\r\n -----\r\n"
        response += "monthly can be used like monthly-1 - monthly-4 . 1 means from start and counts up. 4 means last and counts down from the last day of month.\r\n -----\r\n"
        response += "do remind #random once 24.12.2019 00:00 i have to tell you: santa doesn't exist! :the_horns: \r\n -----------\r\n"
        response += "do remind #random monthly-2 24.12.2020 08:00 The 24. is a thursday, so it will remember you every 2. thursday, 8:00am in month (which will be the 10. dez but not 24. :wink:) \r\n -----------\r\n"
        response += "show reminders \r\n"
        response += "show reminder <id>\r\n"
        response += "delete reminder <id> \r\n"
        response += "wikipedia <topic name> \r\n"
        response += "calc <calculation> \r\n"
        response += "make a joke | tell a joke (append with a space optional 0-2 to get a specific source - else randomised) \r\n \r\n"
    # if command.startswith("calculate ") or command.startswith("calc "):
    elif similar(command, "calculate") > diffPropability or similar(command, "calc") > diffPropability:
        searchSplit = command.split()
        searchSplit.pop(0)
        searchSplit = ''.join(searchSplit)
        response = str(searchSplit) +" = "+str(eval(searchSplit))

    elif similar(command, "say hi") > diffPropability:
        response = "Hey people..\r\n"
        response += "i'm a simple script which can do some stuff :grin: \r\n"
        response += "if you need help, *mention* me and type *help me* additional \r\n"
        response += " :nyancat: \r\n"

    elif similar(command, "make art") > diffPropability:
        #if command.split()[-1]!='art':
            #response = art(command.split()[-1])
        #else:
        response = art("random")

    elif command.startswith("show reminders"):
        response = text2art("Let me look..",font='white_bubble',chr_ignore=False)+"\r\n"
        updateReminders()
        for row in rows:
            response += str(row)+"\r\n"
    elif similar(command, "country") > diffPropability:
        searchSplit = command.split()
        searchSplit.pop(0)
        searchSplit = ' '.join(searchSplit)
        try:
            country = CountryInfo(searchSplit)
            tzSplit = country.timezones()[0].split("+")
            tzDelta = int(tzSplit[1].split(":")[0])*60*60
            tzpwn = tz.tzoffset(tzSplit[0],tzDelta)
            utcnow = datetime.utcnow().replace(tzinfo=tz.tzutc())
            now = utcnow.astimezone(tzpwn)
            response = "Time there: "+str(now.hour)+":"+str(now.minute)+":"+str(now.second)+" "+str(now.date())
            response += "\r\n Wiki: "+country.wiki()+"\r\n"
            response += "Currencies: "+str(country.currencies())+"\r\n"
            response += "Calling codes: "+str(country.calling_codes())+"\r\n"
        except Exception as e:
            print(e)
            if searchSplit.lower() == 'serbia':
                response = text2art("Serbia's",font='white_bubble',chr_ignore=False)+" not included in the used lib yet (https://github.com/porimol/countryinfo/issues/13). However, if you are in belgrade, visit 20/44 :wink: \r\n https://en.wikipedia.org/wiki/Serbia"
            else:
                response = "Can't find "+searchSplit+" - is that a country? :thinking_face:"    
    elif similar(command, "wikipedia") > diffPropability:
        searchSplit = command.split()
        searchSplit.pop(0)
        searchSplit = ' '.join(searchSplit)
        try:
            try:
                w = wikipedia.page(searchSplit)
                print(w.summary)
                response = w.summary
                response += "\r\n"+w.url        
            except wikipedia.exceptions.PageError as e:
                response = "Can't find your wikipedia-page..."
        except Exception as e:
            print(e)
            response = "Something went wrong... it helps to be exact :nerd_face:"
    elif similar(command, "hacked") > diffPropability:
        '''
        searchSplit = command.split(" ")
        searchSplit.pop(0)
        searchSplit = searchSplit[0]
        resp = pyhibp.get_account_breaches(account=simplifyEmailName(searchSplit), truncate_response=True)
        if(resp==False):
            response = "Concrats, it seems your not hacked :sunglasses: \r\n Still, this is not a full check. There's just no *known* hacked data which involves you! :zany_face:"
        else:
            response = ":face_with_symbols_on_mouth: Crap, it seems like your data was exposed, your data is maybe compromised!"
            response += "\r\nAffected services:"
            for services in resp:
                response += "\r\n"+str(services)
        '''
        response = "As https://haveibeenpwned.com wants now money for a api-key, it's the easiest to visit the website and check your mail there."
    elif command.startswith("delete reminder "):
        commandSplit = command.split()
        response = "Delete reminder "+commandSplit[2]+" :bomb:\r\n"
        sql = ''' DELETE FROM reminders WHERE id = ? '''
        conn1 = sqlite3.connect("./botdates.sql")
        conn1.execute(sql,(commandSplit[2]))
        conn1.commit()
        conn1.close()
        updateReminders()
        
    elif command.startswith("show reminder "):
        message = ""
        channel1 = ""
        response = ":eyes: "
        type = ""
        startdate = ""
        starttime = ""
        cmdArr = command.split()
        if(len(cmdArr)>1):
            i = 0
            theRow = None
            for row in rows:
                if row[0] == int(cmdArr[2]):
                    theRow = row # str(row)+"\r\n"
            if theRow == None:
                response += "No row with a such id :persevere:"
            else:        
                # iz = datetime.now()
                type = theRow[3]
                startdate = theRow[4].split()
                startdatesplit = startdate[0].split(".")
                starttimesplit = startdate[1].split(":")
                response += str(theRow)+"\r\n"
                iz = datetime.now()
                denn = datetime(int(startdatesplit[2]),int(startdatesplit[1]),int(startdatesplit[0]))
                lastDayOfMonth = calendar.monthrange(iz.year,iz.month)[1]
                # response += ". U dr letscht tag isch dr "+str(lastDayOfMonth)
                weekDay = datetime(iz.year,iz.month,lastDayOfMonth).weekday()
                # response += ". It's the "+str(weekDay+1)+" day of the week. "
                i = 0
                insert = True
                if(True):
                        if(type.startswith("monthly-")):
                            #
                            spliters = type.split("-")
                            weekPart = spliters[1]
                            response = getMonthResponse(response,weekPart,startdatesplit,iz)
    elif command.startswith("do remind"):
        message = ""
        channel1 = ""
        response = ":+1: "
        type = ""
        startdate = ""
        starttime = ""
        cmdArr = command.split()
        if(len(cmdArr)>2):
                i = 0
                for cmdPart in cmdArr:
                        if(i==2):
                                channel1 = cmdPart
                        if(i==3):
                                type = cmdPart
                        if(i==4):
                                startdate = cmdPart
                        if(i==5):
                                starttime = cmdPart
                        if(i>5):
                                # response += cmdPart+" "
                                message += cmdPart+" "
                        i += 1
                # iz = datetime.now()
                startdatesplit = startdate.split(".")
                starttimesplit = starttime.split(":")
                iz = datetime.now()
                denn = datetime(int(startdatesplit[2]),int(startdatesplit[1]),int(startdatesplit[0]))
                lastDayOfMonth = calendar.monthrange(iz.year,iz.month)[1]
                # response += ". U dr letscht tag isch dr "+str(lastDayOfMonth)
                weekDay = datetime(iz.year,iz.month,lastDayOfMonth).weekday()
                # response += ". It's the "+str(weekDay+1)+" day of the week. "
                i = 0
                insert = True
                #while(weekDay>4):
                        #lastDayOfMonth -= 1
                        #weekDay = datetime(iz.year,iz.month,lastDayOfMonth).weekday()
                        #response += str(lastDayOfMonth)+" is a "
                        #if(weekDay<5):
                        #if(True):
                                #insert = True
                                # response += str(c.lastrowid)

                       #else:
                                #insert = False
                                #response += "Weekend, chips and code!"
                                #c = conn.cursor()
                      #weekDay = datetime(iz.year,iz.month,lastDayOfMonth).weekday()
                #
                if(True):
                        if(type.startswith("monthly-")):
                            #
                            spliters = type.split("-")
                            weekPart = spliters[1]
                            newyearmonth = 1
                            response += getMonthResponse(response,weekPart,startdatesplit,iz)
                        response += "\r\n save reminder and update list"
                        sql = ''' INSERT INTO reminders(message,channel,type,start,author) VALUES(?,?,?,?,?) '''
                        conn2 = sqlite3.connect("./botdates.sql")
                        conn2.execute(sql,(message,channel1,type,str(denn.day)+"."+str(denn.month)+"."+str(denn.year)+" "+starttime,"<@"+data['user']+">"))
                        conn2.commit()
                        conn2.close()
                        updateReminders()
        
        #
        else:
            response = "Sure...write some more code then I can do that! Your cmd was: "+command
    return response
