import os
import traceback
import calendar
from datetime import datetime
from dateutil import tz

import time

import _thread
import re
import sqlite3

import random

import sys
# from slackclient import SlackClient
import slack

from datehelpers import *
from logger import *
from commandparser import *
#from chatterbot import ChatBot
#from chatterbot.trainers import ChatterBotCorpusTrainer, UbuntuCorpusTrainer
# instantiate Slack client
#slack_client = SlackClient(os.environ.get('SLACK_BOT_TOKEN'))
slack_client = slack.WebClient(token=os.environ['SLACK_BOT_TOKEN'])
rtm_client = slack.RTMClient(token=os.environ['SLACK_BOT_TOKEN'])
#rtm_client.start()
threader = None
rtmRun = False
'''
# those lines are needed for enable the AI
bot = ChatBot(
    'Terminal',
    storage_adapter='chatterbot.storage.SQLStorageAdapter',
    logic_adapters=[
        'chatterbot.logic.MathematicalEvaluation',
        'chatterbot.logic.BestMatch'
    ],
    preprocessors=[
        'chatterbot.preprocessors.clean_whitespace'
    ],
    database_uri='sqlite:///ai.db'
)

#ubuntuTrainer = UbuntuCorpusTrainer(bot)
#ubuntuTrainer.train();
bot.initialize()
'''
# starterbot's user ID in Slack: value is assigned after the bot starts up
starterbot_id = None
starterbot_name = None

laughCounter = 0
# constants
RTM_READ_DELAY = 60 # 60 seconds allow to execute the reminders once
MENTION_REGEX = "^<@(|[WU].+?)>(.*)"

@slack.RTMClient.run_on(event='reaction_added')
def get_reaction(**payload):
        response = intepretReaction(payload)
        if response != "":
            response += ' <@'+payload['data']['user']+'>'
            slack_client.chat_postMessage(
                channel=payload['data']['item']['channel'],
                text=response,
                thread_ts=payload['data']['item']['ts']
            )

@slack.RTMClient.run_on(event='open')
def get_team_data(**payload):
    global starterbot_id
    global starterbot_name
    starterbot_id = payload['data']['self']['id']
    starterbot_name = payload['data']['self']['name']
    logger.info("Connected, get my real id")
    logger.info(str(payload['data']['self']['id']))
    team_domain = payload['data']['team']['domain']
    
    
def parse_bot_commands(slack_events):
    """
        Parses a list of events coming from the Slack RTM API to find bot commands.
        If a bot command is found, this function returns a tuple of command and channel.
        If its not found, then this function returns None, None.
    """
    for event in slack_events:
        if event["type"] == "message" and not "subtype" in event:
            user_id, message = parse_direct_mention(event["text"])
            if user_id == starterbot_id:
                return message, event["channel"]
    return None, None

def parse_direct_mention(message_text):
    """
        Finds a direct mention (a mention that is at the beginning) in message text
        and returns the user ID which was mentioned. If there is no direct mention, returns None
    """
    matches = re.search(MENTION_REGEX, message_text)
    # the first group contains the username, the second group contains the remaining message
    return (matches.group(1), matches.group(2).strip()) if matches else (None, None)

def handle_command(command, channel,data):
    response = intepretCommand(command,channel,data)
    # Sends the response back to the channel
    #slack_client.api_call(
    #    "chat.postMessage",
    #    channel=channel,
    #    text=response or default_response
    #)
    # print(str(data))

    if(response is not None and response.startswith("<")):
        response = response[1:-1]
    if 'thread_ts' in data:
        slack_client.chat_postMessage(
            channel=channel,
            thread_ts=data['thread_ts'],
            text=response
        )
    else:
        slack_client.chat_postMessage(
            channel=channel,
            text=str(response)
        )
@slack.RTMClient.run_on(event='message.im')
def onMsgPersonal(**payload):
    logger.debug("pm!!")
    onMsg(payload)    

@slack.RTMClient.run_on(event='message')
def onMsg(**payload):
    global starterbot_id
    global starterbot_name
    data = payload['data']
    if 'user' in data:
        if(data['user']==starterbot_id):
            return
    else:
        return
    channel = data['channel']
    user=data['user']
    command = data['text'].split()
    mention = command.pop(0)

    if(starterbot_id not in mention):
        logger.debug("Not mentioned, ignore")
        return
    command = " ".join(command)
    if command:
        handle_command(command, channel,data)

def doThread():
    sendToSingleUser = False
    # give the webconnection a moment. also works otherwise, but then it's able to print on start
    time.sleep(15)
    logger.debug("start thread")
    while(True):
        # get rows into thread and same time update them
        rows = getRows()
        if rows != None:
            logger.debug("run thread")
            iz = datetime.now()
            for reminder in rows:
                 postIt = intepretReminder(reminder,iz)
                 if(postIt):
                     logger.debug("raw: "+reminder[2])
                     if reminder[2].startswith('<@'):
                         receiversArr = reminder[2].split(',')
                         usedReceiversArr = []
                         for receiver in receiversArr:
                             logger.debug("receiver: "+receiver+", "+simplifyEmailId(receiver))
                             usedReceiversArr.append(simplifyEmailId(receiver))
                         data = slack_client.conversations_open(users=usedReceiversArr)
                         slack_client.chat_postMessage(
                             channel=data['channel']['id'],
                             text=reminder[1]
                        )
                     else:
                         logger.debug("receiver2: "+reminder[2]+", "+simplifyChannelName(reminder[2]))
                         slack_client.chat_postMessage(
                             channel=simplifyChannelName(reminder[2]),
                             text=reminder[1]
                        )
        time.sleep(RTM_READ_DELAY)

if __name__ == "__main__":
    try:
        conn = sqlite3.connect("./botdates.sql")
        c = conn.cursor()
        c.execute("CREATE TABLE IF NOT EXISTS reminders ( id INTEGER PRIMARY KEY, message TEXT NOT NULL,channel TEXT NOT NULL,type text NOT NULL,start text NOT NULL,author text NOT NULL);")
        c.close()
        conn.close()
    except Error as e:
        logger.error(e)
    updateReminders()
    threader = _thread.start_new_thread( doThread, () )
    # for debugging, the while can be a if (=disable autorestart)
    while True:
        if rtmRun is not True:
            try:
                logger.info('Start bot')
                rtmRun = True
                # threader = _thread.start_new_thread( doThread, () )
                rtm_client.start()
            except Exception as e:
                logger.error(e)
                rtmRun = False
                logger.error(''.join(traceback.format_tb(e.__traceback__)))
                # 10s seems a good break to allow a reconnection. stop seems to break reconnection.
                logger.error("Connection failed. Exception traceback printed above. Try again in 10s")
                time.sleep(10)
            # if eg the ethernet-cable is out, it still shouldn't stress the cpu
            time.sleep(2)
